module "label_vpc" {
  source     = "cloudposse/label/null"
  version    = "0.25.0"
  context    = module.base_label.context
  name       = "vpc"
  attributes = ["main"]
}

data "aws_availability_zones" "available" {
  state = "available"
}

resource "aws_vpc" "main" {
  cidr_block           = var.vpc_cidr
  enable_dns_support   = true
  enable_dns_hostnames = true
  tags                 = module.label_vpc.tags
}

# =========================
# Create your subnets here
# =========================

resource "aws_subnet" "subnet-public-1" {
    vpc_id = aws_vpc.main.id
    cidr_block = "192.170.0.0/28"
    availability_zone = data.aws_availability_zones.available.names[0]        
}

resource "aws_subnet" "subnet-private-1" {
    vpc_id = aws_vpc.main.id
    cidr_block = "192.170.0.16/28"
    availability_zone = data.aws_availability_zones.available.names[0]         
}

resource "aws_internet_gateway" "cloudposse-igw" {
    vpc_id = aws_vpc.main.id
    tags = module.label_vpc.tags
}

resource "aws_route_table" "cloudposse-public-rt" {
  vpc_id = aws_vpc.main.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.cloudposse-igw.id
  }  
}

resource "aws_route_table_association" "cloudposse-subnet-public-1" {
  route_table_id = aws_route_table.cloudposse-public-rt.id
  subnet_id = aws_subnet.subnet-public-1.id
}

